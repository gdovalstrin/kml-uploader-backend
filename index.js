const express = require('express');
const busboy = require('connect-busboy'); // Utiliza connect-busboy para Express 4
const fs = require('fs');
const path = require('path');
const cors = require('cors');

const app = express();
const port = 3001;

// Configurar cors para permitir solicitudes desde cualquier origen (*)
app.use(cors());

// Configurar connect-busboy como middleware para manejar la carga de archivos
app.use(busboy());

// Establecer la carpeta para subir archivos
const uploadDir = path.join(__dirname, 'uploads');
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

app.post('/upload', (req, res) => {
  const uploadedFiles = [];

  req.pipe(req.busboy);

  req.busboy.on('file', (fieldname, file, filename) => {
    console.log(filename.filename);
    const saveTo = path.join(uploadDir, filename.filename);
    const writeStream = fs.createWriteStream(saveTo);

    file.pipe(writeStream);

    writeStream.on('close', () => {
      uploadedFiles.push(filename);
    });
  });

  req.busboy.on('finish', () => {
    res.status(200).json({ message: 'Archivos subidos exitosamente', files: uploadedFiles });
  });
});

app.listen(port, () => {
  console.log(`Servidor Express en funcionamiento en el puerto ${port}`);
});
